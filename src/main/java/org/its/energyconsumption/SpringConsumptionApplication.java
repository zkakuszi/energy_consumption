package org.its.energyconsumption;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringConsumptionApplication {
    public static void main(String[] args) {
        SpringApplication.run(SpringConsumptionApplication.class, args);
    }
}
