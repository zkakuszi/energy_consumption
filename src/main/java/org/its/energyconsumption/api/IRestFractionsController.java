package org.its.energyconsumption.api;

import org.its.energyconsumption.api.data.Fraction;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IRestFractionsController {

    ResponseEntity<?> listFractions();

    ResponseEntity<?> listFractionsByProfile(String profileId);

    ResponseEntity<?> createFractions(List<Fraction> fractions);

    ResponseEntity<?> updateFractions(String profileId, List<Fraction> fractions);

    ResponseEntity<?> deleteFracionsByProfile(String profileId);
}
