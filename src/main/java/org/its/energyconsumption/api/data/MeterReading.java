package org.its.energyconsumption.api.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.Validate;
import org.its.energyconsumption.model.MonthEnum;

import java.time.Month;
import java.util.Objects;

public final class MeterReading {

    @ApiModelProperty(example = "0002", notes = "MeterId for meter rading", required = true)
    private final String meterId;
    @ApiModelProperty(example = "A", notes = "Profile of meter reading", required = true)
    private final String profile;
    @ApiModelProperty(example = "JAN", notes = "Month of meter reading", required = true)
    private final MonthEnum month;
    @ApiModelProperty(example = "18", notes = "Actual meter reading", required = true)
    private final int meterReading;

    @JsonCreator
    public MeterReading(@JsonProperty("meterId") final String meterId, @JsonProperty("profile") final String profile, @JsonProperty("month") final MonthEnum month,
                        @JsonProperty("meterReading") final int meterReading) {

        Validate.notNull(meterId, "Field meterId should be set");
        this.meterId = meterId;
        Validate.notNull(profile, "Field profile should be set");
        this.profile = profile;
        Validate.notNull(month, "Field month should be set");
        this.month = month;
        Validate.notNull(meterReading, "Field meterReading should be set");
        this.meterReading = meterReading;
    }

    public String getMeterId() {
        return meterId;
    }

    public String getProfile() {
        return profile;
    }

    public MonthEnum getMonth() {
        return month;
    }

    public int getMeterReading() {
        return meterReading;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MeterReading that = (MeterReading) o;
        return meterReading == that.meterReading
                && Objects.equals(meterId, that.meterId)
                && Objects.equals(profile, that.profile)
                && month == that.month;
    }

    @Override
    public int hashCode() {
        return Objects.hash(meterId, profile, month, meterReading);
    }

    @Override
    public String toString() {
        return "MeterReading{" +
                "meterId='" + meterId + '\'' +
                ", profile='" + profile + '\'' +
                ", month=" + month +
                ", meterReading=" + meterReading +
                '}';
    }
}
