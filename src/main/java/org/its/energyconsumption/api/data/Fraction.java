package org.its.energyconsumption.api.data;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import org.apache.commons.lang3.Validate;
import org.its.energyconsumption.model.MonthEnum;

import java.util.Objects;

/**
 * POJO to store API data.
 */
public final class Fraction {

    @ApiModelProperty(example = "JAN", notes = "Month of fraction", required = true)
    private final MonthEnum month;
    @ApiModelProperty(example = "A", notes = "Profile of fraction", required = true)
    private final String profile;
    @ApiModelProperty(example = "0.56", notes = "Actual fraction", required = true)
    private final double fraction;

    @JsonCreator
    public Fraction(@JsonProperty("month") final MonthEnum month, @JsonProperty("profile") final String profile,
                    @JsonProperty("fraction")final double fraction) {
        Validate.notNull(month, "Field month should be set");
        this.month = month;
        Validate.notNull(profile, "Field fraction should be set");
        this.profile = profile;
        Validate.notNull(fraction, "Field profile should be set");
        this.fraction = fraction;
    }

    public MonthEnum getMonth() {
        return month;
    }

    public String getProfile() {
        return profile;
    }

    public double getFraction() {
        return fraction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Fraction fraction1 = (Fraction) o;
        return Double.compare(fraction1.fraction, fraction) == 0
                && month == fraction1.month
                && Objects.equals(profile, fraction1.profile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(month, profile, fraction);
    }

    @Override
    public String toString() {
        return "Fraction{" +
                "month=" + month +
                ", profile='" + profile + '\'' +
                ", fraction=" + fraction +
                '}';
    }
}
