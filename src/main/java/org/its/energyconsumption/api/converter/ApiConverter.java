package org.its.energyconsumption.api.converter;

import org.its.energyconsumption.model.Fraction;
import org.its.energyconsumption.model.MeterReading;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Converter class to convert data coming from API to domain models.
 */
@Service
public class ApiConverter {

    public List<Fraction> convertApiFractionFrom(List<org.its.energyconsumption.api.data.Fraction> fractions) {
        return fractions != null ? fractions.stream()
                .map(e -> new Fraction(e.getMonth(), e.getProfile(), e.getFraction()))
                .collect(Collectors.toList()) : Collections.emptyList();
    }

    public List<org.its.energyconsumption.api.data.Fraction> convertApiFractionTo(List<Fraction> fractions) {
        return fractions != null ?fractions.stream()
                .map(e -> new org.its.energyconsumption.api.data.Fraction(e.getMonth(), e.getProfile(), e.getFraction()))
                .collect(Collectors.toList()) : Collections.emptyList();
    }

    public List<MeterReading> convertApiMeterReadingFrom(List<org.its.energyconsumption.api.data.MeterReading> meterReadings) {
        return meterReadings != null ? meterReadings.stream()
                .map(e -> new MeterReading(e.getMeterId(), e.getProfile(), e.getMonth(), e.getMeterReading()))
                .collect(Collectors.toList()) : Collections.emptyList();
    }

    public List<org.its.energyconsumption.api.data.MeterReading> convertApiMeterReadingTo(List<MeterReading> meterReadings) {
        return meterReadings != null ? meterReadings.stream()
                .map(e -> new org.its.energyconsumption.api.data.MeterReading(e.getMeterId(), e.getProfile(), e.getMonth(), e.getMeterReading()))
                .collect(Collectors.toList()) : Collections.emptyList();
    }

    public org.its.energyconsumption.api.data.MeterReading convertApiSingleMeterReadingTo(MeterReading meterReading) {
        return new org.its.energyconsumption.api.data.MeterReading(meterReading.getMeterId(), meterReading.getProfile(),
                meterReading.getMonth(), meterReading.getMeterReading());
    }
}
