package org.its.energyconsumption.api;

import org.its.energyconsumption.model.MonthEnum;
import org.springframework.http.ResponseEntity;

public interface IRestConsumptionController {

    ResponseEntity<?> listConsumptionsByMeterId(String meterId, MonthEnum month);

    ResponseEntity<?> listConsumptionsByProfile(String profileId, MonthEnum month);
}
