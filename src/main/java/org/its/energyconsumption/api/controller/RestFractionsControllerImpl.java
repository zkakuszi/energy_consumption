package org.its.energyconsumption.api.controller;

import io.swagger.annotations.ApiOperation;
import org.its.energyconsumption.api.IRestFractionsController;
import org.its.energyconsumption.api.converter.ApiConverter;
import org.its.energyconsumption.api.data.Fraction;
import org.its.energyconsumption.service.FractionsService;
import org.its.energyconsumption.service.exception.FractionsValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * Rest service endpoint for manage fractions.
 */
@Controller
@RequestMapping("/api/v2/ec")
public class RestFractionsControllerImpl implements IRestFractionsController {

    @Autowired
    private FractionsService fractionsService;
    @Autowired
    private ApiConverter apiConverter;

    @Override
    @ApiOperation(value = "List all fractions")
    @RequestMapping(value = "/fractions", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listFractions() {
        List<Fraction> fractions;

        try {
            fractions = apiConverter.convertApiFractionTo(fractionsService.listFractions());
        } catch (FractionsValidationException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<>(fractions, getResponseHeader(), HttpStatus.OK);
    }

    @Override
    @ApiOperation(value = "List fractions by profileId")
    @RequestMapping(value = "/fractions/{profileId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listFractionsByProfile(@PathVariable("profileId") String profileId) {
        List<Fraction> fractions;
        try {
            fractions = apiConverter.convertApiFractionTo(fractionsService.listFractionsByProfile(profileId));
        } catch (FractionsValidationException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<List<Fraction>>(fractions, getResponseHeader(), HttpStatus.OK);
    }

    @Override
    @ApiOperation(value = "Insert fractions")
    @RequestMapping(value = "/fractions/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createFractions(@RequestBody List<Fraction> fractions) {
        try {
            fractionsService.createFractions(apiConverter.convertApiFractionFrom(fractions));
        }  catch (FractionsValidationException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<String>(getResponseHeader(), HttpStatus.CREATED);
    }

    @Override
    @ApiOperation(value = "Update fractions by profileId")
    @RequestMapping(value = "/fractions/{profileId}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<?> updateFractions(@PathVariable("profileId") String profileId, @RequestBody List<Fraction> fractions) {
        try {
            fractionsService.updateFractions(profileId, apiConverter.convertApiFractionFrom(fractions));
        }  catch (FractionsValidationException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<String>(getResponseHeader(), HttpStatus.OK);
    }

    @Override
    @ApiOperation(value = "Delete fractions by profileId")
    @RequestMapping(value = "/fractions/{profileId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<?> deleteFracionsByProfile(@PathVariable("profileId") String profileId) {
        try {
            fractionsService.deleteFracions(profileId);
        }  catch (FractionsValidationException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<String>(getResponseHeader(), HttpStatus.NO_CONTENT);
    }

    private HttpHeaders getResponseHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

        return httpHeaders;
    }
}
