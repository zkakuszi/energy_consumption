package org.its.energyconsumption.api.controller;

import io.swagger.annotations.ApiOperation;
import org.its.energyconsumption.api.IRestConsumptionController;
import org.its.energyconsumption.api.converter.ApiConverter;
import org.its.energyconsumption.api.data.MeterReading;
import org.its.energyconsumption.model.MonthEnum;
import org.its.energyconsumption.service.ConsumptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/api/v2/ec")
public class RestConsumptionControllerImpl implements IRestConsumptionController {

    @Autowired
    private ConsumptionService consumptionService;
    @Autowired
    private ApiConverter apiConverter;

    @Override
    @ApiOperation(value = "List consumptions for month by meterId")
    @RequestMapping(value = "/consumption/{meterId}/{month}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listConsumptionsByMeterId(@PathVariable("meterId") String meterId, @PathVariable("month") MonthEnum month) {
        MeterReading meterReading;

        try {
            meterReading = apiConverter.convertApiSingleMeterReadingTo(
                    consumptionService.getMeterReadingMonthByMeterId(meterId, month));
        } catch (RuntimeException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<>(meterReading.getMeterReading(), getResponseHeader(), HttpStatus.OK);
    }

    @Override
    @ApiOperation(value = "List consumptions for month by profileId")
    @RequestMapping(value = "/consumption/{profileId}/{month}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listConsumptionsByProfile(@PathVariable("profileId") String profileId, @PathVariable("meterId") MonthEnum month) {
        MeterReading meterReading;

        try {
            meterReading = apiConverter.convertApiSingleMeterReadingTo(
                    consumptionService.getMeterReadingMonthByProfileId(profileId, month));
        } catch (RuntimeException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<>(meterReading.getMeterReading(), getResponseHeader(), HttpStatus.OK);
    }

    private HttpHeaders getResponseHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

        return httpHeaders;
    }
}

