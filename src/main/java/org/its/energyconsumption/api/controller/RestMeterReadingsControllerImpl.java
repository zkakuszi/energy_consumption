package org.its.energyconsumption.api.controller;

import io.swagger.annotations.ApiOperation;
import org.its.energyconsumption.api.IRestMeterReadingsController;
import org.its.energyconsumption.api.converter.ApiConverter;
import org.its.energyconsumption.api.data.MeterReading;
import org.its.energyconsumption.service.MeterReadingsService;
import org.its.energyconsumption.service.exception.MeterReadingsValidationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Rest service endpoint for manage meter readings.
 */
@Controller
@RequestMapping("/api/v2/ec")
public class RestMeterReadingsControllerImpl implements IRestMeterReadingsController {

    @Autowired
    private MeterReadingsService meterReadingsService;
    @Autowired
    private ApiConverter apiConverter;

    @Override
    @ApiOperation(value = "List all meter readings.")
    @RequestMapping(value = "/meterreadings", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listMeterReadings() {

        List<MeterReading> meterReadings;

        try {
            meterReadings = apiConverter.convertApiMeterReadingTo(meterReadingsService.listMeterReadings());
        } catch (MeterReadingsValidationException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<>(meterReadings, getResponseHeader(), HttpStatus.OK);
    }

    @Override
    @ApiOperation(value = "List meter radings by meterId")
    @RequestMapping(value = "/meterreadings/{meterId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseEntity<?> listMeterReadingsByMeterId(@PathVariable("meterId") String meterId) {
        List<MeterReading> meterReadings;

        try {
            meterReadings = apiConverter.convertApiMeterReadingTo(meterReadingsService.listMeterReadingsByMeterId(meterId));
        } catch (MeterReadingsValidationException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<List<MeterReading>>(meterReadings, getResponseHeader(), HttpStatus.OK);
    }

    @Override
    @ApiOperation(value = "Insert meter readings")
    @RequestMapping(value = "/meterreadings/", method = RequestMethod.POST, produces = "application/json")
    public ResponseEntity<?> createMeterReadings(@RequestBody List<MeterReading> meterReadings) {
        try {
            meterReadingsService.createMeterReadings(apiConverter.convertApiMeterReadingFrom(meterReadings));
        }  catch (MeterReadingsValidationException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<String>(getResponseHeader(), HttpStatus.CREATED);
    }

    @Override
    @ApiOperation(value = "Update meter readings by meterId")
    @RequestMapping(value = "/meterreadings/{meterId}", method = RequestMethod.PUT, produces = "application/json")
    public ResponseEntity<?> updateMeterReadings(@PathVariable("meterId") String meterId, @RequestBody List<MeterReading> meterReadings) {
        try {
            meterReadingsService.updateMeterReadings(meterId, apiConverter.convertApiMeterReadingFrom(meterReadings));
        }  catch (MeterReadingsValidationException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<String>(getResponseHeader(), HttpStatus.OK);
    }

    @Override
    @ApiOperation(value = "Delete meter radings by meterId")
    @RequestMapping(value = "/meterreadings/{meterId}", method = RequestMethod.DELETE, produces = "application/json")
    public ResponseEntity<?> deleteMeterReadingsByMeterId(@PathVariable("meterId") String meterId) {
        try {
            meterReadingsService.deleteMeterReadings(meterId);
        }  catch (MeterReadingsValidationException e) {
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(e.getMessage());
        }

        return new ResponseEntity<String>(getResponseHeader(), HttpStatus.NO_CONTENT);
    }

    private HttpHeaders getResponseHeader() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);

        return httpHeaders;
    }
}
