package org.its.energyconsumption.api;

import org.its.energyconsumption.api.data.MeterReading;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface IRestMeterReadingsController {

    ResponseEntity<?> listMeterReadings();

    ResponseEntity<?> listMeterReadingsByMeterId(String meterId);

    ResponseEntity<?> createMeterReadings(List<MeterReading> meterReadings);

    ResponseEntity<?> updateMeterReadings(String meterId, List<MeterReading> meterReadings);

    ResponseEntity<?> deleteMeterReadingsByMeterId(String meterId);
}
