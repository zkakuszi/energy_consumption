package org.its.energyconsumption.model;

import java.util.Objects;

/**
 * POJO to represent a fraction.
 */
public final class Fraction {
    private final MonthEnum month;
    private final String profile;
    private final double fraction;

    public Fraction(final MonthEnum month, final String profile, final double fraction) {
        this.month = month;
        this.profile = profile;
        this.fraction = fraction;
    }

    public MonthEnum getMonth() {
        return month;
    }

    public String getProfile() {
        return profile;
    }

    public double getFraction() {
        return fraction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Fraction fraction1 = (Fraction) o;
        return Double.compare(fraction1.fraction, fraction) == 0
                && month == fraction1.month
                && Objects.equals(profile, fraction1.profile);
    }

    @Override
    public int hashCode() {
        return Objects.hash(month, profile, fraction);
    }

    @Override
    public String toString() {
        return "Fraction{" +
                "month=" + month +
                ", profile='" + profile + '\'' +
                ", fraction=" + fraction +
                '}';
    }
}

