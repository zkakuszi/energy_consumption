package org.its.energyconsumption.model;

import java.util.Objects;

/**
 * POJO to store meter reading.
 */
public final class MeterReading {
    private final String meterId;
    private final String profile;
    private final MonthEnum month;
    private final int meterReading;

    public MeterReading(final String meterId, final String profile, final MonthEnum month, final int meterReading) {
        this.meterId = meterId;
        this.profile = profile;
        this.month = month;
        this.meterReading = meterReading;
    }

    public String getMeterId() {
        return meterId;
    }

    public String getProfile() {
        return profile;
    }

    public MonthEnum getMonth() {
        return month;
    }

    public int getMeterReading() {
        return meterReading;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MeterReading that = (MeterReading) o;
        return meterReading == that.meterReading
                && Objects.equals(meterId, that.meterId)
                && Objects.equals(profile, that.profile)
                && month == that.month;
    }

    @Override
    public int hashCode() {
        return Objects.hash(meterId, profile, month, meterReading);
    }

    @Override
    public String toString() {
        return "MeterReading{" +
                "meterId='" + meterId + '\'' +
                ", profile='" + profile + '\'' +
                ", month=" + month +
                ", meterReading=" + meterReading +
                '}';
    }
}

