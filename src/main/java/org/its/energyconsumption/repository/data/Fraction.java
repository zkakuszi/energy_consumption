package org.its.energyconsumption.repository.data;

import org.its.energyconsumption.model.MonthEnum;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 * POJO to represent a fraction for database layer.
 */
@Entity
public final class Fraction {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private MonthEnum month;
    private String profile;
    private double fraction;

    public Fraction() {}

    public Fraction(Long id, final MonthEnum month, final String profile, final double fraction) {
        this.id = id;
        this.month = month;
        this.profile = profile;
        this.fraction = fraction;
    }

    public Fraction(final MonthEnum month, final String profile, final double fraction) {
        this.month = month;
        this.profile = profile;
        this.fraction = fraction;
    }

    public Long getId() {
        return id;
    }

    public MonthEnum getMonth() {
        return month;
    }

    public String getProfile() {
        return profile;
    }

    public double getFraction() {
        return fraction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Fraction fraction1 = (Fraction) o;
        return Double.compare(fraction1.fraction, fraction) == 0
                && Objects.equals(id, fraction1.id)
                && month == fraction1.month
                && Objects.equals(profile, fraction1.profile);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, month, profile, fraction);
    }

    @Override
    public String toString() {
        return "Fraction{" +
                "id=" + id +
                ", month=" + month +
                ", profile='" + profile + '\'' +
                ", fraction=" + fraction +
                '}';
    }
}

