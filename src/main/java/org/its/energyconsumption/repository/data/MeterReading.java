package org.its.energyconsumption.repository.data;

import org.its.energyconsumption.model.MonthEnum;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Objects;

/**
 * POJO to represent Meter reading for database layer.
 */
@Entity
public final class MeterReading {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private final String meterId;
    private final String profile;
    private final MonthEnum month;
    private final int meterReading;

    public MeterReading(final Long id, final String meterId, final String profile, final MonthEnum month, final int meterReading) {
        this.meterId = meterId;
        this.profile = profile;
        this.month = month;
        this.meterReading = meterReading;
    }

    public MeterReading(final String meterId, final String profile, final MonthEnum month, final int meterReading) {
        this.meterId = meterId;
        this.profile = profile;
        this.month = month;
        this.meterReading = meterReading;
    }

    public Long getId() {
        return id;
    }

    public String getMeterId() {
        return meterId;
    }

    public String getProfile() {
        return profile;
    }

    public MonthEnum getMonth() {
        return month;
    }

    public int getMeterReading() {
        return meterReading;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MeterReading that = (MeterReading) o;
        return meterReading == that.meterReading
                && Objects.equals(id, that.id)
                && Objects.equals(meterId, that.meterId)
                && Objects.equals(profile, that.profile)
                && month == that.month;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, meterId, profile, month, meterReading);
    }

    @Override
    public String toString() {
        return "MeterReading{" +
                "id=" + id +
                ", meterId='" + meterId + '\'' +
                ", profile='" + profile + '\'' +
                ", month=" + month +
                ", meterReading=" + meterReading +
                '}';
    }
}
