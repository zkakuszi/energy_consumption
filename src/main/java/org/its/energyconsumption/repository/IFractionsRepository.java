package org.its.energyconsumption.repository;

import org.its.energyconsumption.repository.data.Fraction;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IFractionsRepository extends CrudRepository<Fraction, Long> {

    List<Fraction> findByProfile(String profile);

    <S extends Fraction> Iterable<S> saveAll(Iterable<S> var1);

    List<Fraction> findAll();

    void deleteAll(Iterable<? extends Fraction> var1);
}
