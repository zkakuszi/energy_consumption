package org.its.energyconsumption.repository.converter;

import org.its.energyconsumption.model.Fraction;
import org.its.energyconsumption.model.MeterReading;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to convert Meter readings related pojos from database to pojos in business layer.
 */
@Component
public class MeterReadingsConverter {

    public List<MeterReading> convertDataTo(List<org.its.energyconsumption.repository.data.MeterReading> meterReadings) {
        return meterReadings.stream()
                .map(e -> new MeterReading(e.getMeterId(), e.getProfile(), e.getMonth(), e.getMeterReading()))
                .collect(Collectors.toList());
    }

    public List<org.its.energyconsumption.repository.data.MeterReading> convertDataFrom(List<MeterReading> meterReadings) {
        return meterReadings.stream()
                .map(e -> new org.its.energyconsumption.repository.data.MeterReading(e.getMeterId(), e.getProfile(), e.getMonth(), e.getMeterReading()))
                .collect(Collectors.toList());
    }

    public List<org.its.energyconsumption.repository.data.MeterReading> convertDataFromWithId(List<org.its.energyconsumption.repository.data.MeterReading> fractionsToUpdate,
                                                                                          List<MeterReading> meterReadingsToSave) {
        List<org.its.energyconsumption.repository.data.MeterReading> meterReadingsToUpdateWithId = new ArrayList<>();

        for (int i = 0; i < fractionsToUpdate.size(); i++) {
            meterReadingsToUpdateWithId.add(new org.its.energyconsumption.repository.data.MeterReading(fractionsToUpdate.get(i).getId(),
                    meterReadingsToSave.get(i).getMeterId(),
                    meterReadingsToSave.get(i).getProfile(),
                    meterReadingsToSave.get(i).getMonth(),
                    meterReadingsToSave.get(i).getMeterReading()));
        }

        return meterReadingsToUpdateWithId;
    }
}
