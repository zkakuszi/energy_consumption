package org.its.energyconsumption.repository.converter;

import org.its.energyconsumption.model.Fraction;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to convert Fractions related pojos from database to pojos in business layer.
 */
@Component
public class FractionsConverter {

    public List<Fraction> convertDataTo(List<org.its.energyconsumption.repository.data.Fraction> fractions) {
        return fractions.stream()
                .map(e -> new Fraction(e.getMonth(), e.getProfile(), e.getFraction()))
                .collect(Collectors.toList());
    }

    public List<org.its.energyconsumption.repository.data.Fraction> convertDataFrom(List<Fraction> fractions) {
        return fractions.stream()
                .map(e -> new org.its.energyconsumption.repository.data.Fraction(e.getMonth(), e.getProfile(), e.getFraction()))
                .collect(Collectors.toList());
    }

    public List<org.its.energyconsumption.repository.data.Fraction> convertDataFromWithId(List<org.its.energyconsumption.repository.data.Fraction> fractionsToUpdate,
                                                                                          List<Fraction> fractionsToSave) {
        List<org.its.energyconsumption.repository.data.Fraction> fractionToUpdateWithId = new ArrayList<>();

        for (int i = 0; i < fractionsToUpdate.size(); i++) {
            fractionToUpdateWithId.add(new org.its.energyconsumption.repository.data.Fraction(fractionsToUpdate.get(i).getId(),
                    fractionsToSave.get(i).getMonth(),
                    fractionsToSave.get(i).getProfile(),
                    fractionsToSave.get(i).getFraction()));
        }

        return fractionToUpdateWithId;
    }
}
