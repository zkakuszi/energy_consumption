package org.its.energyconsumption.repository;

import org.its.energyconsumption.repository.data.MeterReading;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface IMeterReadingsRepository extends CrudRepository<MeterReading, Long> {

    List<MeterReading> findByMeterId(String meterId);

    List<MeterReading> findByProfile(String profileId);

    List<MeterReading> findAll();

    <S extends MeterReading> Iterable<S> saveAll(Iterable<S> var1);

    void deleteAll(Iterable<? extends MeterReading> var1);
}
