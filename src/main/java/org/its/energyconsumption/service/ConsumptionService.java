package org.its.energyconsumption.service;

import org.its.energyconsumption.model.MeterReading;
import org.its.energyconsumption.model.MonthEnum;
import org.its.energyconsumption.repository.IMeterReadingsRepository;
import org.its.energyconsumption.repository.converter.MeterReadingsConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsumptionService {

    @Autowired
    private IMeterReadingsRepository meterReadingsRepository;
    @Autowired
    private MeterReadingsConverter meterReadingsConverter;

    public MeterReading getMeterReadingMonthByProfileId(String profileId, MonthEnum monthEnum) {
        List<MeterReading> meterReadings = meterReadingsConverter.convertDataTo(meterReadingsRepository.findByProfile(profileId));

        return meterReadings.stream()
                .filter(e -> e.getMonth().getMonthValue() == monthEnum.getMonthValue())
                .findFirst()
                .get();
    }

    public MeterReading getMeterReadingMonthByMeterId(String meterId, MonthEnum monthEnum) {
        List<MeterReading> meterReadings = meterReadingsConverter.convertDataTo(meterReadingsRepository.findByMeterId(meterId));

        return meterReadings.stream()
                .filter(e -> e.getMonth().getMonthValue() == monthEnum.getMonthValue())
                .findFirst()
                .get();
    }
}
