package org.its.energyconsumption.service;

import org.its.energyconsumption.model.Fraction;
import org.its.energyconsumption.model.MeterReading;
import org.its.energyconsumption.repository.IFractionsRepository;
import org.its.energyconsumption.repository.IMeterReadingsRepository;
import org.its.energyconsumption.repository.converter.FractionsConverter;
import org.its.energyconsumption.repository.converter.MeterReadingsConverter;
import org.its.energyconsumption.service.exception.MeterReadingsValidationException;
import org.its.energyconsumption.service.validation.MeterReadingsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MeterReadingsService {

    @Autowired
    private IMeterReadingsRepository meterReadingsRepository;
    @Autowired
    private IFractionsRepository fractionsRepository;
    @Autowired
    private MeterReadingsConverter meterReadingsConverter;
    @Autowired
    private FractionsConverter fractionsConverter;
    @Autowired
    private MeterReadingsValidator meterReadingsValidator;


    public List<MeterReading> listMeterReadings() {
        return meterReadingsConverter.convertDataTo(meterReadingsRepository.findAll());
    }

    public List<MeterReading> listMeterReadingsByMeterId(String meterId) {
        return meterReadingsConverter.convertDataTo(meterReadingsRepository.findByMeterId(meterId));
    }

    public void createMeterReadings(List<MeterReading> meterReadings) {
        List<Fraction> fractions = fractionsConverter.convertDataTo(fractionsRepository.findAll());
        Map<String, String> invalidMeterIdsWithErrorMessages = meterReadingsValidator.validateMeterReadings(meterReadings, fractions);

        if (!invalidMeterIdsWithErrorMessages.isEmpty()) {
            removeInvalidDataAndSave(invalidMeterIdsWithErrorMessages.keySet(), meterReadings);
            throw new MeterReadingsValidationException(invalidMeterIdsWithErrorMessages);
        }

        meterReadingsRepository.saveAll(meterReadingsConverter.convertDataFrom(meterReadings));
    }

    public void updateMeterReadings(String meterId, List<MeterReading> meterReadings) {
        List<Fraction> fractions = fractionsConverter.convertDataTo(fractionsRepository.findAll());
        Map<String, String> invalidMeterIdsWithErrorMessages = meterReadingsValidator.validateMeterReadings(meterReadings, fractions);

        if (!invalidMeterIdsWithErrorMessages.isEmpty()) {
            removeInvalidDataAndSave(invalidMeterIdsWithErrorMessages.keySet(), meterReadings);
            throw new MeterReadingsValidationException(invalidMeterIdsWithErrorMessages);
        }

        meterReadingsRepository.saveAll(meterReadingsConverter.convertDataFromWithId(meterReadingsRepository.findByMeterId(meterId), meterReadings));
    }

    public void deleteMeterReadings(String meterId) {
        meterReadingsRepository.deleteAll(meterReadingsRepository.findByMeterId(meterId));
    }

    private void removeInvalidDataAndSave(Set<String> invalidMeterIds, List<MeterReading> meterReadings) {
        List<MeterReading> validMeterReadings = meterReadings.stream()
                .filter(e -> !invalidMeterIds.contains(e.getMeterId()))
                .collect(Collectors.toList());

        meterReadingsRepository.saveAll(meterReadingsConverter.convertDataFrom(validMeterReadings));

    }
}
