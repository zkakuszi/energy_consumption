package org.its.energyconsumption.service.exception;

/**
 * Class to represent internal exception type for validation.
 */
public class FractionsValidationException extends RuntimeException {

    public FractionsValidationException(String errorMessage) {
        super(errorMessage);
    }

}
