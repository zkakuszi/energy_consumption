package org.its.energyconsumption.service.exception;

/**
 * Class to represent a single meter reading validation exception.
 * This is for internal usage only.
 */
public class SingleMeterReadingValidationException extends RuntimeException {
    private final String meterId;
    private final String errorMessage;

    public SingleMeterReadingValidationException(String meterId, String errorMessage) {
        super(errorMessage);
        this.meterId = meterId;
        this.errorMessage = errorMessage;
    }

    public String getMeterId() {
        return meterId;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
