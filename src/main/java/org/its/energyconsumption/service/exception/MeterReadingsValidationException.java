package org.its.energyconsumption.service.exception;

import java.util.Map;

/**
 * Class to represent internal validation exception.
 */
public class MeterReadingsValidationException extends RuntimeException {
    private final Map<String, String> invalidIdsWithErrorMessages;

    public MeterReadingsValidationException(Map<String, String> invalidIdsWithErrorMessages) {
        this.invalidIdsWithErrorMessages = invalidIdsWithErrorMessages;
    }

    public Map<String, String> getIdsWithMessages() {
        return invalidIdsWithErrorMessages;
    }
}
