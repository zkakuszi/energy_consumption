package org.its.energyconsumption.service;

import org.its.energyconsumption.model.Fraction;
import org.its.energyconsumption.repository.IFractionsRepository;
import org.its.energyconsumption.repository.converter.FractionsConverter;
import org.its.energyconsumption.service.validation.FractionsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FractionsService {

    @Autowired
    private IFractionsRepository fractionsRepository;
    @Autowired
    private FractionsConverter fractionsConverter;
    @Autowired
    private FractionsValidator fractionsValidator;

    public List<Fraction> listFractions() {
        return fractionsConverter.convertDataTo(fractionsRepository.findAll());
    }

    public List<Fraction> listFractionsByProfile(String profile) {
        return fractionsConverter.convertDataTo(fractionsRepository.findByProfile(profile));
    }

    public void createFractions(List<Fraction> fractions) {
        fractionsValidator.validateFracions(fractions);
        fractionsRepository.saveAll(fractionsConverter.convertDataFrom(fractions));
    }

    // upserting existing franctions by id
    public void updateFractions(String profile, List<Fraction> fractions) {
        fractionsValidator.validateFractionsForSingleProfile(profile, fractions);
        fractionsRepository.saveAll(fractionsConverter.convertDataFromWithId(fractionsRepository.findByProfile(profile), fractions));
    }

    public void deleteFracions(String profile) {
        fractionsRepository.deleteAll(fractionsRepository.findByProfile(profile));
    }
}
