package org.its.energyconsumption.service.validation;

import org.its.energyconsumption.model.Fraction;
import org.its.energyconsumption.service.exception.FractionsValidationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Class to validate fractions flows.
 */
@Component
public class FractionsValidator {

    private static final double PROFILE_RATIO_SUM_VALID = 1;
    private static final int NUMBER_OF_MONTH_IN_YEAR = 12;
    private static final String PROFILE_RATIO_VALIDATION_MESSAGE = "Profile ratio sum must be 1 for profile %s";
    private static final String FRACTIONS_SHOULD_COVER_A_YEAR = "For given profile %s, number of fracions should cover a full year";

    private static final Logger LOGGER = LoggerFactory.getLogger(FractionsValidator.class);

    public void validateFracions(List<Fraction> fractions) {
        fractions.stream()
                .map(Fraction::getProfile)
                .collect(Collectors.toSet())
                .forEach(e -> {
                    validateFractionsForSingleProfile(e, fractions);
                });
    }

    public void validateFractionsForSingleProfile(String profileId, List<Fraction> fractions) {
        List<Fraction> fractionsForSingleProfile = getFracionsForProfile(profileId, fractions);

        if (fractionsForSingleProfile.size() != NUMBER_OF_MONTH_IN_YEAR) {
            handleError(FRACTIONS_SHOULD_COVER_A_YEAR, profileId);
        } else if (!profileRatioValid(fractionsForSingleProfile)) {
            handleError(PROFILE_RATIO_VALIDATION_MESSAGE, profileId);
        }
    }

    private List<Fraction> getFracionsForProfile(String profileId, List<Fraction> fractions) {
        return fractions.stream()
                .filter(e -> e.getProfile().equals(profileId))
                .collect(Collectors.toList());
    }

    private boolean profileRatioValid(List<Fraction> fractions) throws FractionsValidationException {
        return fractions.stream()
                .map(Fraction::getFraction)
                .mapToDouble(Double::doubleValue)
                .sum() == PROFILE_RATIO_SUM_VALID;
    }

    private void handleError(String errorMessagePattern, String profileId) {
        String errorMessage = String.format(errorMessagePattern, profileId);
        LOGGER.error(errorMessage);
        throw new FractionsValidationException(errorMessage);
    }
}
