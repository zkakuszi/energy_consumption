package org.its.energyconsumption.service.validation;

import org.apache.commons.lang3.Range;
import org.its.energyconsumption.model.Fraction;
import org.its.energyconsumption.model.MeterReading;
import org.its.energyconsumption.model.MonthEnum;
import org.its.energyconsumption.service.exception.SingleMeterReadingValidationException;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Class to validate meter readings endpoints.
 */
@Component
public class MeterReadingsValidator {

    private static final String READING_SHOULD_BE_EQUAL_OR_HIGHER_THEN_PREVIOUS = "Reading for %s should be higher than previous month for meter %s";
    private static final String FRACTIONS_EXISTENCE_FOR_METER_READING = "Fractions should already exist for profile %s, please define it first";
    private static final String INVALID_CONSISTENCY_FRACTIONS_TOLERANCE = "Inconsistent consumption (%s) with meterId %s for month %s";
    private static Comparator<MeterReading> meterReadingsComparator;
    private static final double CONSUMPTION_TOLERANCE_VALUE = 0.25;

    public Map<String, String> validateMeterReadings(List<MeterReading> meterReadings, List<Fraction> fractions) {
        Map<String, String> invalidMeterIdsWithErrorMessages = Collections.emptyMap();

        meterReadings
                .stream()
                .map(MeterReading::getMeterId)
                .collect(Collectors.toSet())
                .forEach(meterId -> {
                    try {
                        List<MeterReading> orderedReadingsByMeterId = getValidatedOrder(getMeterReadingByMeterId(meterId, meterReadings));
                        validateFractionsExistence(meterId, orderedReadingsByMeterId, fractions);
                        validateConsumptionTolerance(meterId, getTotalConsumptionForYear(orderedReadingsByMeterId), orderedReadingsByMeterId, fractions);
                    } catch (SingleMeterReadingValidationException e) {
                        invalidMeterIdsWithErrorMessages.put(e.getMeterId(), e.getErrorMessage());
                    }

                });

        return invalidMeterIdsWithErrorMessages;
    }

    private List<MeterReading> getValidatedOrder(List<MeterReading> meterReadings) {
        return meterReadings.stream()
                .sorted(meterReadingsComparator)
                .collect(Collectors.toList());
    }

    private void validateFractionsExistence(String meterId, List<MeterReading> meterReadings, List<Fraction> fractions) {
        String profileId = meterReadings.stream()
                .map(MeterReading::getProfile)
                .findFirst()
                .get();

        boolean match = fractions.stream()
                .anyMatch(e -> e.getProfile().equals(profileId));

        if (!match) {
            throw new SingleMeterReadingValidationException(meterId,
                    String.format(FRACTIONS_EXISTENCE_FOR_METER_READING, profileId));
        }
    }

    private void validateConsumptionTolerance(String meterId, int yearConsumption, List<MeterReading> meterReadings, List<Fraction> fractions) {
        fractions
                .forEach(e -> {
                    double expectedConsumption = yearConsumption * e.getFraction();
                    Integer actualConsumption = getConsumptionPerMonth(e.getMonth(), meterReadings);
                    boolean validForTolerance = Range.between(expectedConsumption - expectedConsumption * CONSUMPTION_TOLERANCE_VALUE,
                            expectedConsumption + expectedConsumption * CONSUMPTION_TOLERANCE_VALUE)
                            .contains(Double.valueOf(actualConsumption));
                    if (!validForTolerance) {
                        throw new SingleMeterReadingValidationException(meterId,
                                String.format(INVALID_CONSISTENCY_FRACTIONS_TOLERANCE, actualConsumption, meterId, e.getMonth()));
                    }
                });
    }

    private int getConsumptionPerMonth(MonthEnum monthEnum, List<MeterReading> meterReadings) {
        if (MonthEnum.JAN.equals(monthEnum)) {
            return meterReadings.get(0).getMeterReading();
        } else {
            return meterReadings.get(monthEnum.getMonthValue() - 1).getMeterReading() - meterReadings.get(monthEnum.getMonthValue() - 2).getMeterReading();
        }
    }

    private List<MeterReading> getMeterReadingByMeterId(String meterId, List<MeterReading> meterReadings) {
        return meterReadings.stream()
                .filter(e -> e.getMeterId().equals(meterId))
                .collect(Collectors.toList());
    }

    private int getTotalConsumptionForYear(List<MeterReading> meterReadings) {
        return meterReadings.stream()
                .map(MeterReading::getMeterReading)
                .mapToInt(Integer::intValue)
                .sum();
    }

    static {
        meterReadingsComparator  = (o1, o2) -> {
            int monthCompareVal =  o1.getMonth().getMonthValue() - o2.getMonth().getMonthValue();
            if (monthCompareVal < 0 && (o1.getMeterReading() - o2.getMeterReading()) < 0) {
                throw new SingleMeterReadingValidationException(o1.getMeterId(),
                        String.format(READING_SHOULD_BE_EQUAL_OR_HIGHER_THEN_PREVIOUS, o1.getMonth().getMonthValue(), o1.getMeterId()));
            }

            return monthCompareVal;
        };
    }

}
